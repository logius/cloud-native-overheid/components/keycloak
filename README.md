# Keycloak

In CNO, [Keycloak](https://www.keycloak.org) is used to implement authentication.

__Platform Administrators__ use Keycloak to authenticate other platform administrators, application administrators and developers on CNO-components (GitLab, Harbor, ...).

__Application Administrators__ use Keycloak to authenticate end-users on their applications.

## Sources and links

- [Keycloak chart](https://github.com/codecentric/helm-charts/tree/master/charts/keycloak)

# Contents

This project defines an Ansible role for deploying Keycloak.

Prerequisites:

- [CertManager](https://gitlab.com/logius/cloud-native-overheid/components/certmanager)
- [Nginx Ingress Controller](https://gitlab.com/logius/cloud-native-overheid/components/nginx-ingress)

In this project:

- Local installation, test and development (all optional)
- Datacenter installation
- Design notes
- Trouble shooting

Local installation assumes the [platform-runner](https://gitlab.com/logius/cloud-native-overheid/components/platform-runner) to run in a Docker image.

# Local installation

Get platform runner:
```
docker pull registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest
```

Connect to Kubernetes cluster, e.g.:
```
export KUBECONFIG=$HOME/.kube/config
```

In working directory, add or extend `cluster_config.yaml`, e.g.:
```
platform:
  prefix: "local"
  domain: example.com # Use your own domain, ref. certmanager

config:
  certmanager:
    domain:  example.com
    issuer: example-issuer # Use your own domain prefix instead of "example"

  keycloak:
    replicas: 1
    test_ldap_account: true # Todo: reconsider name:value
```

From working directory, install Keycloak:
```
# Prepare Ansible command, assuming above cluster_config.yaml will be mounted below
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/keycloak \
  -vv"

# Run Ansible role, using ANSIBLE_CMD with mounted cluster config
docker run --rm \
  -v $KUBECONFIG:/home/builder/.kube/config \
  -v $PWD/cluster_config.yaml:/playbook/configfiles/cluster_config.yaml \
  -e ANSIBLE_FORCE_COLOR=true \
  --network=host \
  registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
  $ANSIBLE_CMD
```

The installation uses dependencies as defined in `./meta/main.yml`.

The reading order for Ansible parameterization is: `defaults/main.yml`, overlayed with `$PWD/cluster_config.yaml`.

# Local test

Smoke test, unit test, integration test:

## Smoke test

Check PostgreSQL deployment:
```
kubectl config set-context --current --namespace=local-keycloak

# PGPASSWORD is PostgreSQL default, ref. https://www.postgresql.org/docs/current/libpq-envars.html
export PGUSERNAME=$(kubectl get secret postgresql-user -o jsonpath={.data.username} | base64 --decode)
export PGPASSWORD=$(kubectl get secret postgresql-user -o jsonpath={.data.password} | base64 --decode)

POD=$(kubectl get pods | grep postgresql-11 | awk '{print $1}')

# Access Pod
kubectl exec -it ${POD} -- env "PGUSERNAME=${PGUSERNAME}" env "PGPASSWORD=${PGPASSWORD}" /bin/bash

# In Pod
psql -d keycloak -U ${PGUSERNAME} # Psql uses PGPASSWORD as default password
\l
\c keycloak
\dt
\q
```

Check Keycloak deployment:
```
kubectl config set-context --current --namespace=local-keycloak
kubectl get statefulsets
kubectl get pvc
kubectl get pods
kubectl get secrets
```

## Unit test

Perform the earlier `docker run`, now using:
```
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/keycloak \
  -t test \
  -vv"
```

## Integration test

Add Keycloak ip# and hostname to `/etc/hosts`, e.g.:
```
export WORKER_IP=$(kubectl get nodes -o wide | grep "worker .*Ready" | awk '{print $6}'); echo ${WORKER_IP}
sudo echo "${WORKER_IP} keycloak.example.com" | sudo tee -a /etc/hosts # Use your own domain
```

At commandline, test the Keycloak hostname (mind the ending "/"):
```
curl https://keycloak.example.com/auth/ # Use your own domain
```

As admin, in the browser, login at Keycloak using the same URL, with context path `/auth/admin`, using credentials:
```
kubectl config set-context --current --namespace=local-keycloak
export KKUSERNAME=$(kubectl get secret keycloak-http -o jsonpath={.data.username} | base64 --decode); echo ${KKUSERNAME}
export KKPASSWORD=$(kubectl get secret keycloak-http -o jsonpath={.data.password} | base64 --decode); echo ${KKPASSWORD}
```

As admin, in Keycloak, observe that the following realms are present:

- The default realm `Master`
- The custom added realm (e.g. `Platform`)

When users are subsequently created in the custom realm (say in `Platform`), they can login at the corresponding context path (say at `/auth/admin/platform/console`).

# Local development

Checkout this project, e.g.:
```
git clone git@gitlab.com:logius/cloud-native-overheid/components/keycloak.git
```

Checkout projects for dependent roles as defined in `$PWD/keycloak/meta/main.yml`:
```
git clone git@gitlab.com:logius/cloud-native-overheid/components/common.git
```

Develop and install, this time with `ANSIBLE_ROLES_PATH` and `galaxy_url` referring to local directory:
```
export ANSIBLE_ROLES_PATH=$PWD

ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=keycloak \
  -vv"

docker run --rm \
  -v $KUBECONFIG:/home/builder/.kube/config \
  -v $PWD/cluster_config.yaml:/playbook/configfiles/cluster_config.yaml \
  -v $ANSIBLE_ROLES_PATH:/playbook/roles \
  -e ANSIBLE_FORCE_COLOR=true \
  --network=host \
  registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
  $ANSIBLE_CMD
```

Optional cleanup using:
```
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=keycloak \
  -t remove \
  -vv"
```

__Attention__: avoid the cleanup when you are about to install other CNO components subsequently.

# Datacenter installation

In your own CI/CD, create a pipeline similar to the local installation.

# Design notes

Contents:

- Overall
- Helm charts
- Ingress
- Realm
- Service account
- LDAP
- API server
- Backup
- High Availability

## Overall

Keycloak is installed with a PostgreSQL database.

Connecting to an already existing LDAP is optional.

## Helm charts

Keycloak is installed from a standard [Keycloak Helm Chart](https://github.com/codecentric/helm-charts/tree/master/charts/keycloak).

PostgreSQL is installed from a standard [PostgreSQL Helm Chart](https://github.com/bitnami/charts/tree/master/bitnami/postgresql).

A single PostgreSQL server is installed with a Bitnami image. The installation is not hardened yet. The plan is to migrate the database to a PostgreSQL cluster using the Crunchy Data operator.

For security purposes, the involved images are ideally obtained from a private registry.

## Ingress

There are two Keyloack ingresses: `admin ingress` and `external ingress`.

### Admin ingress

The admin ingress is intended for access by platform/application administrators and for access by APIs.

This ingress:
- is typically accessed from an internal network;
- is disclosed by an internal loadbalancer;
- is not disclosed by an external (internet) load balancer.

### External ingress

The ``external ingress`` is intended for access by platform/application users, when authenticating on any CNO platform component.

This ingress:

- is typically accessed from an external network;
- is not disclosed by an internal loadbalancer;
- is disclosed by an external (internet) loadbalancer;

## Realm

The standard Keycloak realm `Master` is used for administration purposes.

In addition, a custom realm is added for usage by CNO platform components.

The custom realm is configured from a `./templates/default-realm.json` (fairly default), or from a `.json` that is specified in the environment variable `REALM_FILEPATH`.

The `REALM_FILEPATH` can be used when installing the CNO platform in your own datacenter.

Currently, the realm is added as part of this keycloak role. It might be better to put this functionality in a separate role, in order to support multiple realms.

## Service Account

In the realm, a service account is registered with the name `platform-cli`.

The credentials for this account are saved to a secret (`platform-cli`) in the Keycloak namespace.

Other roles that need to have access to the Keycloak APIs (e.g. to register a client), are allowed to use this service account. See also [CNO common](https://gitlab.com/logius/cloud-native-overheid/components/common/-/tree/master/keycloak/tasks).

The granted permissions for the platform-cli are `create-client`, `manage-clients` and `view-clients`. These permissions may need to be extended in the future, if we develop Ansible roles with more functionality with regard to Keycloak.

## LDAP

_This option can be ignored for a local installation._

The realm has optional support for an LDAP connection.

The credentials for connecting to the LDAP server must be provided in the secret `keycloak-ldap-credential`, in the `config.keycloak.namespace`. The credentials stored in the secret should be provided by the administrator of the LDAP server.

If the `keycloak-ldap-credential` secret is unavailable, the LDAP connection is ignored.

The LDAP server may have private certificates which are not trusted by default. For this reason a truststore is created using Java's keytool and provided to Keycloak server from a mounted K8s secret.

## API server

_This option can be ignored for a local installation._

Access to the Kubernetes API server from Kubectl can be authenticated via Keycloak (consider to ignore in local installation).

For each K8s cluster that is integrated with Keycloak using OIDC, a client is registered in the realm, using configuration in the config file.

In the future we need to make it easier to add / remove K8s clusters. The creation of the K8s client might become a separate role or tag.

## Backup

_This option can be ignored for a local installation._

A Velero backup schedule will be created, but only when a given secret with access to a S3 site(s) is present.

Backups schedules are created for each S3 site defined in the S3 secret.

See also `configure_backups.yml` in [CNO common](https://gitlab.com/logius/cloud-native-overheid/components/common).

## High Availability

_This option can be ignored for a local installation._

CNO Keycloak has features for backup and failover between an active and a passive (standby) site.

There are two main scenarios:

- Planned Migration: the active site is put in maintenance mode, the passive site becomes active. In this proces, the data is transfered from active to standby with backups. The backup is a Velero namespace backup including the PVC of the database. In the planned migration the external ingress is removed to disconnected the active site from the loadbalancer. The passive site is activated by enabling the ingress and restoring the data.
- Disaster Recovery: the active site goes down. The standby site will be promoted to active. The last available backup is restored.

Backups are only created in the active site.

In the `./tasks/recovery/recovery_jobs.yml`, multiple tags are provided to support these scenarios.

For a disaster recovery plan, there must also be a procedure to make sure there will never be two active sites. This exercise is not handled by the ansible role. It must be handled by the admin user or by the load balancer.

# Troubleshooting

## Failure on task: Get keycloak API access token

Observe: task failure.

Cause: this can occur when the CertManager did not deliver a Let's Encrypt certificate yet.
```
kubectl config set-context --current --namespace=local-keycloak
kukbectl get Certificate # Observe false in this case
```

Solution: just wait some minutes and re-run the installation.

## Unable to get managed connection for java:jboss/datasources/KeycloakDS

Observe: Keycloak logfile shows this exception.

Cause: this can occur when installing Keycloak on an older Kubernetes version (1.16).

Solution: use latest Kubernets version (last tested on 1.19).

## User limit of inotify instances reached or too many open files

Observe: Keycloak logfile shows exception:
```
java.lang.RuntimeException: java.io.IOException: User limit of inotify instances reached or too many open files\n\tat org.jboss.xnio.nio@3.8.1.Final
```

Cause/solution:
```
# https://stackoverflow.com/questions/32281277/too-many-open-files-failed-to-initialize-inotify-the-user-limit-on-the-total
cat /proc/sys/fs/inotify/max_user_instances # 128
sudo su
echo 256 > /proc/sys/fs/inotify/max_user_instances
cat /proc/sys/fs/inotify/max_user_instances # 256
```

## Connect PostgreSQL client from localhost to PostgreSQL service

Useful for exploring the PostgreSQL database.

Steps:

- Install psql.

- Set port forward to service
```
kubectl get services # Observe postgresql-11
kubectl port-forward svc/postgresql-11 5432:5432
```

- Set database credentials
```
export PGUSERNAME=$(kubectl get secret postgresql-user -o jsonpath={.data.username} | base64 --decode)
export PGPASSWORD=$(kubectl get secret postgresql-user -o jsonpath={.data.password} | base64 --decode)
```

- Connect to database, using exported password as default:
```
psql -h 127.0.0.1 -U $PGUSERNAME
```

- Explore databases:
```
\l
```
