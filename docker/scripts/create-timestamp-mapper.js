// Get LDAP attribute createTimestamp
var timestamp = user.attributes.createTimestamp[0];
// Take the date part without the Z indicator 
timestamp = timestamp.substr(0, 14);
// parse to int 
var id = parseInt(timestamp);
// render long 
id.longValue(); 

