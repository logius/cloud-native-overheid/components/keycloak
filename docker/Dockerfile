#
# KeyCloak optimized image, see https://www.keycloak.org/server/containers
# 
# The optimized image performs the build step to make the start of the container faster. 
# Also, the image contains jars for metrics and scripts, and includes the truststore. 
# 

# Script jar builder
FROM alpine:latest as script-builder
RUN apk add --update zip

# Javascripts
ADD scripts /tmp/scripts
RUN zip /scripts.jar /tmp/scripts/*

# KeyCloak builder
FROM quay.io/keycloak/keycloak:18.0.0 as keycloak-builder

ENV KC_CACHE=ispn
ENV KC_CACHE_STACK=kubernetes
ENV KC_DB=postgres
ENV KC_FEATURES=scripts
ENV KC_HEALTH_ENABLED=true
ENV KC_HTTP_RELATIVE_PATH=/auth
ENV KC_METRICS_ENABLED=true 
ENV KC_PROXY=edge

# Truststore 
ADD trust /tmp/trust
RUN keytool -importcert -noprompt -v -trustcacerts -file /tmp/trust/*.crt -keystore /opt/keycloak/conf/cacerts -storepass nosecret;

# Metrics 
RUN curl -sL https://github.com/aerogear/keycloak-metrics-spi/releases/download/2.5.3/keycloak-metrics-spi-2.5.3.jar -o /opt/keycloak/providers/keycloak-metrics-spi-2.5.3.jar

# Build
RUN /opt/keycloak/bin/kc.sh build 

## KeyCloak Runtime image 
FROM quay.io/keycloak/keycloak:18.0.0
COPY --from=keycloak-builder /opt/keycloak/ /opt/keycloak/
COPY --from=script-builder /*.jar /opt/keycloak/providers
WORKDIR /opt/keycloak

ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start"]

