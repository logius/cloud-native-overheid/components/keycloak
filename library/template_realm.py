# 
# Clean a realm JSON file and prepare it for use as a template in the keycloak role. 
# 
# - Remove technical IDs (id and containerId)
# - Remove clients except the default clients
# - Remove roles and groups except the default roles and groups
# - Create placeholders for the realm name 
# - Optionally remove the LDAP section
# 
from ansible.module_utils.basic import AnsibleModule

import json

def init_ansible_module():
    return AnsibleModule(
        argument_spec=dict(
            realm=dict(type='dict', required=True),
            dest=dict(type='str', required=True),
            ldap=dict(type='bool', required=False, default=False),
        ),
        supports_check_mode=True
    )

def remove_ids(realm):

    if 'id' in realm: 
        del realm['id'] 
    if 'containerId' in realm: 
        del realm['containerId']

    for key, value in realm.items():
        if isinstance(value, dict):
            remove_ids(value)
        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    remove_ids(item)

accepted_clients = [ 
    'account', 
    'account-console', 
    'admin-cli', 
    'broker', 
    'realm-management', 
    'security-admin-console' ]

def cleanup_clients(realm, realm_id):
    
    remaining_clients = []
    for client in realm['clients']:
        if client['clientId'] in accepted_clients: 
            remaining_clients.append(client)
            client['secret'] = "{{ lookup('password', '/dev/null length=20 chars=ascii_letters') }}"
            if 'baseUrl' in client: 
                client['baseUrl'] = client['baseUrl'].replace('/' + realm_id, '/{{ config.keycloak.realm }}')
            if 'redirectUris' in client: 
                new_uris = []
                for uri in client['redirectUris']:
                    new_uris.append(uri.replace('/' + realm_id, '/{{ config.keycloak.realm }}'))
                client['redirectUris'] = new_uris

    realm['clients'] = remaining_clients

def cleanup_roles(realm):
    remaining_role_clients = {}
    for client, data in realm['roles']['client'].items():
        if client in accepted_clients: 
            remaining_role_clients[client] = data      

    realm['roles']['client'] = remaining_role_clients

def cleanup_groups(realm):
    accepted_groups = [ 'lpc_developer', 'lpc_beheerder' ]
    
    remaining_groups = []
    for group in realm['groups']:
        if group['name'] in accepted_groups: 
            remaining_groups.append(group)
    realm['groups'] = remaining_groups

def realm_placeholders(realm):

    realm['id'] = realm['realm'] = "{{ config.keycloak.realm }}"
    realm['attributes']['displayName'] = realm['displayName'] = "{{ config.keycloak.display_name }}"
    realm['attributes']['displayNameHtml'] = realm['displayNameHtml'] = "{{ config.keycloak.display_name }}"

    if 'realm' in realm['roles']: 
        for role in realm['roles']['realm']:
            role['containerId'] = "{{ config.keycloak.realm }}"

    if 'org.keycloak.storage.UserStorageProvider' in realm['components']: 
        provider = realm["components"]['org.keycloak.storage.UserStorageProvider'][0]
        provider['name'] = "{{ config.system.cluster_name }}"
        provider['config']['bindCredential'][0] = "{{ bindCredential }}"
        provider['config']['connectionUrl'][0] = "{{ config.keycloak.ldap.url }}"

def remove_ldap(realm):
    if 'org.keycloak.storage.UserStorageProvider' in realm['components']:
        del realm['components']['org.keycloak.storage.UserStorageProvider']

def remove_smtp(realm):
    if 'smtpServer'in realm: 
        del realm['smtpServer']

def main():
    
    module = init_ansible_module()

    realm = module.params.get('realm')

    cleanup_clients(realm, realm['id'])
    cleanup_groups(realm)
    cleanup_roles(realm)
    remove_ids(realm)
    realm_placeholders(realm)
    remove_smtp(realm)
    if not module.params.get('ldap'): 
        remove_ldap(realm)

    filename = module.params.get('dest')
   
    with open(filename, 'w') as file:
        json.dump(realm, file, indent=2, sort_keys=True)

    module.exit_json(changed=True, file=filename, ldap=module.params.get('ldap'))

if __name__ == '__main__':
    main()