#
# Platform-cli Client
#
# The platform-cli client has a service account, which is used by other scripts, to create new OIDC clients.
# The service account in platform-cli uses Client Credentials Grant:
# https://github.com/keycloak/keycloak-documentation/blob/master/server_admin/topics/clients/oidc/service-accounts.adoc#_client-credentials
#
- name: Get Keycloak root user access
  include_tasks: get_keycloak_credentials.yml
  vars:
    new_token_retries: 10

- name: Gather facts for secret of the platform_cli client.
  k8s_info:
    api_version: v1
    kind: Secret
    name: platform-cli
    namespace: "{{ config.keycloak.namespace }}"
  register: platform_cli_secret
  no_log: "{{ config.system.hide_secrets_from_log }}"

- name: Get the OIDC client secret for the platform_cli client.
  set_fact:
    oidc_client_secret: "{{ platform_cli_secret.resources[0].data['client-secret'] | b64decode }}"
  when: platform_cli_secret.resources | length > 0
  no_log: "{{ config.system.hide_secrets_from_log }}"

- name: Create OIDC client secret for the platform_cli client when missing
  block:
    - name: Create OIDC secret
      set_fact:
        oidc_client_secret: "{{ lookup('password', '/dev/null length=20 chars=ascii_letters') }}"

    - name: Store secret in K8s
      shell: |
        kubectl create secret -n {{ config.keycloak.namespace }} generic platform-cli --from-literal=client-secret={{ oidc_client_secret }} \
        --dry-run -o yaml | kubectl apply -n {{ config.keycloak.namespace }} -f -
  when: platform_cli_secret.resources | length == 0
  no_log: "{{ config.system.hide_secrets_from_log }}"

- name: Create Platform client in realm {{ config.keycloak.realm }}
  keycloak_client:
    auth_client_id: admin-cli
    auth_keycloak_url: https://{{ config.keycloak.fqdn }}/auth
    auth_realm: master
    auth_username: "{{ keycloak_secret['username'] }}"
    auth_password: "{{ keycloak_secret['password'] }}"
    redirect_uris: "https://{{ config.keycloak.fqdn }}/oauth2/callback"
    realm: "{{ config.keycloak.realm }}"
    client_authenticator_type: "client-secret"
    client_id: platform-cli
    secret: "{{ oidc_client_secret }}"
    fullScopeAllowed: true
    # Don't support standard flow as we require only client credentials grant.
    standard_flow_enabled: false
    # The next flag will trigger to create a service account.
    service_accounts_enabled: true

    validate_certs: "{{ config.system.validate_certs }}"
    state: present

- name: Get client UUID from realm-management
  uri:
    url: https://{{ config.keycloak.fqdn  }}/auth/admin/realms/{{ config.keycloak.realm }}/clients?clientId=realm-management
    method: GET
    headers:
      Authorization: "Bearer {{ keycloak_tokens.json.access_token }}"
    validate_certs: "{{ config.system.validate_certs }}"
  register: realm_management_id

- name: Get client UUID from platform-cli
  uri:
    url: https://{{ config.keycloak.fqdn  }}/auth/admin/realms/{{ config.keycloak.realm }}/clients?clientId=platform-cli
    method: GET
    headers:
      Authorization: "Bearer {{ keycloak_tokens.json.access_token }}"
    validate_certs: "{{ config.system.validate_certs }}"
  register: platform_cli_id

  # The service account for 'platform-cli' is a user in the realm with the username 'service-account-platform-cli'
- name: Get user UUID from service account in platform-cli
  uri:
    url: https://{{ config.keycloak.fqdn  }}/auth/admin/realms/{{ config.keycloak.realm }}/clients/{{ platform_cli_id.json[0].id }}/service-account-user
    method: GET
    headers:
      Authorization: "Bearer {{ keycloak_tokens.json.access_token }}"
    validate_certs: "{{ config.system.validate_certs }}"
  register: service_account_platform_cli

- name: Assign roles to service account of the platform-cli
  include_tasks: assign_role.yml
  vars:
    - rolename: "{{ item }}"
    - realm_management_uuid: "{{ realm_management_id.json[0].id }}"
    - service_account_platform_cli_uuid: "{{ service_account_platform_cli.json.id }}"
  loop:
    - "create-client"
    - "manage-clients"
    - "manage-realm"
    - "manage-users"
    - "query-users"
    - "view-clients"
